// Polyfills
// fixes for non ES6 browsers (IE, safari, ...)
import 'es6-shim/es6-shim.js';
// todo remove once https://github.com/angular/angular/issues/6501 is fixed.
import './shims/shims_for_IE.js';
import "zone.js/dist/zone";
import "reflect-metadata";

// Angular 2
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/router-deprecated';
import '@angular/http';

// RxJS
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
